<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

            <!-- Fonte do Google -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto" rel="stylesheet">

<!-- CSS Bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="/css/styles.css">
    <script src="/js/scripts.js"></script>
<!-- CSS da aplicação -->

</head>
<body>  
    <header>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="collapse navbar-collapse" id="navbar">
                <a href="/" class="navbar-brand">
                    <img src="/img/imobwil.png" alt="IMOB WILL" width=50px>
                </a>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="/empresa" class="nav-link">A Empresa</a>
                    </li>
                    <li class="nav-item">
                        <a href="/buscar" class="nav-link">Buscar Imóveis</a>
                    </li>
                    <li class="nav-item">
                        <a href="/imoveis/cadastrar" class="nav-link">Cadastre Seu Imóvel</a>
                    </li>
                    <li class="nav-item">
                        <a href="/trabalheconosco" class="nav-link">Trabalhe Conosco</a>
                    </li>
                    <li class="nav-item">
                        <a href="/contato" class="nav-link">Contato</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    @yield('content')
    <footer>
    <a href="/"> Voltar para a HOME</a>
    <br>
    <p>IMOB WILL &copy; 2023</p>
    </footer>
    <script src="https://unpkg.com/ionicons@5.1.2/dist/ionicons.js"></script>
</body>
</html>
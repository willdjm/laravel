<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImovelController extends Controller
{
    public function index(){
        $nome ="Will";
    
        return view('welcome', ['nome' => $nome]);
    }

    public function cadastrar(){
        return view('imoveis.cadastrar');
    }
}

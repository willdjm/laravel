<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Use App\Http\Controllers\ImovelController;

Route::get('/', [ImovelController::class, 'index']);
Route::get('/imoveis/cadastrar', [ImovelController::class, 'cadastrar']);


Route::get('/contato', function () {
    return view('contato');
});

Route::get('/buscar', function () {
    return view('buscar');
});

Route::get('/empresa', function () {
    return view('empresa');
});

Route::get('/trabalheconosco', function () {
    return view('trabalheconosco');
});


